import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { LOGIN_USER } from '../../../src/config';

const email = 'cobacoba@gmail.com';
const pass = 'cobacoba12';

const loginApi = async (em, password) => {
  try {
    return await axios.post(LOGIN_USER, { em, password });
  } catch (e) {
    return [];
  }
};

describe('lOGIN', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  describe('when API call is successful', () => {
    it('Login success', async () => {
      const loginSuccess = {
        user: {
          role: 'user',
          isEmailVerified: true,
          email: 'cobacoba@gmail.com',
          name: 'Axel',
          id: '624966c0bbe9dd7fb4f84504',
        },
      };
      mock.onPost(LOGIN_USER).reply(200, loginSuccess);

      const result = await loginApi(email, pass);

      expect(result.data).toEqual(loginSuccess);
    });
  });

  describe('when API call is unsuccessful', () => {
    it('Login failed', async () => {
      mock.onPost(LOGIN_USER).reply(400, []);
      const result = await loginApi(email, pass);
      expect(result).toEqual([]);
    });
  });
});
