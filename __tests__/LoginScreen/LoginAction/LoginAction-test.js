import ReduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import {
  login, loginLoading, logout, loginUser,
} from '../../../src/redux/action';
import { showSuccess } from '../../../src/utils';

jest.mock('axios');

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

test('Login Action Test', () => {
  expect(login('token', 'name')).toEqual({
    type: '@LOGIN',
    payload: 'token',
    name: 'name',
  });
  expect(loginLoading(true)).toEqual({
    type: '@LOGIN_LOADING',
    payload: true,
  });
  expect(logout()).toEqual({
    type: '@LOGOUT',
  });
});

describe('Login User Test', () => {
  const middlewares = [ReduxThunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore({});

  // Login Success
  test('should Login a  user ', async () => {
    axios.post.mockImplementation(() => Promise.resolve({
      status: 200,
      data: {
        token: 'testToken',
        name: 'testName',
      },
    }));

    const testUser = {
      email: 'test@email.com',
      password: 'testPassword',
    };

    await store.dispatch(loginUser(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(
        loginLoading(true),
        login('testToken', 'testName'),
        showSuccess('Login Sukses'),
      );
    });
  });

  // Login fail
  test('should not Login a  user', async () => {
    axios.mockRejectedValue({
      status: 500,
    });
    const userInfo = {
      name: '',
      email: '',
    };

    await store.dispatch(loginUser(userInfo)).then(() => {
      expect(store.getActions()[0]).toEqual(
        loginLoading(true),
        loginLoading(false),
      );
    });
  });
});
