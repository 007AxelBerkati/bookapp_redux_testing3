import React from 'react';
import { StyleSheet, View } from 'react-native';
import Video from 'react-native-video';
import { IconButton } from '../../component';
import { colors } from '../../utils';
import bunny from '../../assets/video/bunny.mp4';

function VideoScreen({ navigation }) {
  return (
    <View style={styles.page}>
      <IconButton type="back" onPress={() => navigation.goBack()} />
      <View style={styles.videoWrapper}>
        <Video
          ignoreSilentSwitch="ignore"
          resizeMode="cover"
          source={bunny}
          controls
          style={styles.backgroundVideo}
          poster="https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/English_Cocker_Spaniel_4.jpg/800px-English_Cocker_Spaniel_4.jpg"
        />
      </View>
    </View>
  );
}

export default VideoScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    padding: 12,
  },
  backgroundVideo: {
    width: '100%',
    height: 300,
  },
  videoWrapper: {
    paddingTop: 50,
  },
});
