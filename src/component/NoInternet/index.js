import {
  StyleSheet, Text, View, StatusBar,
} from 'react-native';
import React from 'react';
import { colors, fonts } from '../../utils';

function NoInternet() {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      <View style={styles.Container}>
        <Text style={styles.Title}>Connection Error</Text>
        <Text style={styles.Text}>
          Oops! Looks like your device is not connected to the Internet.
        </Text>
      </View>
    </View>
  );
}

export default NoInternet;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: colors.background.primary,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },

  Container: {
    backgroundColor: colors.background.primary,
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 40,
    alignItems: 'center',
  },
  Title: {
    fontSize: 22,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  Text: {
    fontSize: 18,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    marginTop: 14,
    textAlign: 'center',
    marginBottom: 10,
  },
});
